# Social meta data

## Preview

- [Facebook share](/res/adblockplus.org/screenshots/social-meta-data.png)

## Contents

| Page attribute | Required | Default
| :-- | :-- | :-- |
| `og_image` | No | [Featured image](/res/adblockplus.org/static/featured-image.png)
